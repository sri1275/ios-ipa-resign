# README #

This code allow you to resign your own ipa assuming that you have:
1) a developer certificate issued by apple and added to your keychain
2) a mobileprovision file

This code allow you to resign your app without using xcode or if you need to add a UDID for development distribution.
This code correctly signs ipas with Frameworks (.framework folders), Plugins (.appex folders), Applications (.app folders).
This code autoincludes entitlements with binaries extracting them from the provided mobileprovision file.

Usage.
This code runs on mac osx
You should already have installed OSX Command Lines Tools
The code is a shell script

Step 1
Change the following variables inside the signall.sh script:


```
#!shell

signscript="/path/to/sign.sh"
ipasourcefolder="path/to/ipas/source/folder"
ipadestfolder="/path/to/ipas/destinations/folder/"
developer1="iPhone Developer: xxxxx (xxxxx)"
mobileprovision1="/path/to/mobile/provision"
```



Step 2
make sure that ipasourcefolder and ipadestfolder are writable.
run signall.sh via terminal.
done.

In your destination folder you will have all your ipas signed.