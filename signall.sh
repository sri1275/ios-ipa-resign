# !/bin/bash
signscript="/Users/admin/Desktop/sign.sh"
ipasourcefolder="/Users/admin/Desktop/orig"
ipadestfolder="/Users/admin/Desktop/signed/"

developer1="iPhone Developer: xxxx (xxxx)"
mobileprovision1="/Users/admin/Desktop/mobileprovision"


cd $ipasourcefolder
find -d . -type f -name "*.ipa"> files.txt
while IFS='' read -r line || [[ -n "$line" ]]; do
	filename=$(basename "$line" .ipa)
	echo "Ipa: $filename"
	#_dev1_______
	output=$ipadestfolder$filename
	output+="_signed_dev1.ipa"
	"$signscript" "$line" "$developer1" "$mobileprovision1" "$output"

done < files.txt
rm files.txt